import React, { useState, useEffect } from 'react';
import './CommunitySection.css';


export default function CommunitySection() {

const [communityData, setCommunityData] = useState(null);
const [isVisible, setIsVisible] = useState(true);

const toggleVisibility = () => {
    setIsVisible(!isVisible);
}


useEffect(() => {
    fetch('/community')
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json();
    })
    .then(data => setCommunityData(data))
    .catch(error => {
      console.error('Error fetching community data:', error);
    });
}, []);



  function Card({avatar, firstName, lastName, position}) {
    return (
        <div className="card">
            <img src={avatar} />
            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.</h3>
            <p className="name">{`${firstName} ${lastName}`}</p>
            <p className="position">{position}</p>
        </div>
    );
}

// const cards = [
//     {
//         imageSrc: '/path/to/image1.jpg',
//         title: 'Card Title 1',
//         name: 'NAME ONE',
//         position: 'Position One',
//     },
//     {
//         imageSrc: '/path/to/image2.jpg',
//         title: 'Card Title 2',
//         name: 'NAME TWO',
//         position: 'Position Two',
//     },
//     {
//         imageSrc: '/path/to/image3.jpg',
//         title: 'Card Title 3',
//         name: 'NAME THREE',
//         position: 'Position Three',
//     },
// ];

  return (
    <div>
        <div className="community-section">
            <h1>Big Community of People Like You</h1>
            <h2>We’re proud of our products, and we’re really excited when we get feedback from our users.</h2>
            <div className={`cards ${isVisible ? '' : 'hidden'}`}>
                {communityData && communityData.map((card) => (
                    <Card key={card.id} {...card} />
                ))}
            </div>
        </div>

        <button onClick={toggleVisibility} className="toggle-btn">
                {isVisible ? 'Hide section' : 'Show section'}
            </button>
    </div>
  );

}

import React from 'react';
import './App.css';
import CommunitySection from './CommunitySection';
import SubscribeSection from './SubscribeSection';

function App() {
  return (
    <div className="App">
      <CommunitySection />
      <SubscribeSection />
    </div>
  );
}

export default App;

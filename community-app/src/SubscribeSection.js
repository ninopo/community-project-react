import React, { useState } from 'react';
import './SubscribeSection.css';

export default function SubscribeSection() {

const [email, setEmail] = useState('');
const [isLoading, setIsLoading] = useState(false);
const [isSubscribed, setIsSubscribed] = useState(false);


const handleSubscribe = async (email) => {
    if (isLoading) return;

    if(typeof email !== "string") {
        console.error("Email must be a string. Received:", email);
        return;
    }

    if(email === "forbidden@gmail.com") {
        window.alert("Email is already in use");
        return; 
    }

    setIsLoading(true);

    try {
        const response = await fetch('/subscribe', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email })
        });

        if (response.status === 422) {
            const data = await response.json();
            throw new Error(data.error);
        }

        const data = await response.json();
        console.log('Subscribed successfully!', data);
        setEmail('');
        setIsSubscribed(true);
        setIsLoading(false);
    } catch(error) {
        window.alert(error.message);
        setIsLoading(false);
    }
};

const handleUnsubscribe = async () => {
    if (isLoading) return;

    if(typeof email !== "string") {
        console.error("Email must be a string. Received:", email);
        return;
    }

    setIsLoading(true);

    try {
        const response = await fetch('/unsubscribe', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email })
        });

        if (!response.ok) {
            throw new Error("Error unsubscribing");
        }

        const data = await response.json();
        console.log('Unsubscribed successfully!', data);
        setIsSubscribed(false);
        setIsLoading(false);
    } catch(error) {
        window.alert(error.message);
        setIsLoading(false);
    }
};



  return (
    <div className="subscribe-section">
    <h1>Join Our Program</h1>
    <h2>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h2>
    <div className="input-wrapper">
    {isSubscribed ? (
            <button onClick={handleUnsubscribe}
            disabled={isLoading}
            style={isLoading ? {opacity: 0.5} : {}}>Unsubscribe</button>
        ) : (
            <>
                <input 
                    type="email" 
                    value={email} 
                    onChange={(e) => setEmail(e.target.value)} 
                    placeholder="Email"
                    disabled={isLoading}
                />
                <button onClick={() => handleSubscribe(email)}
                disabled={isLoading} 
                style={isLoading ? {opacity: 0.5} : {}}>Subscribe</button>
            </>
        )}
    </div>
</div>
  );
} 